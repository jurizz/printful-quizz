-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2018 at 12:14 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quizz`
--

-- --------------------------------------------------------

--
-- Table structure for table `math_answers`
--

CREATE TABLE `math_answers` (
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` smallint(6) NOT NULL DEFAULT '0',
  `answer_id` int(11) NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `math_answers`
--

INSERT INTO `math_answers` (`test_id`, `question_id`, `is_correct`, `answer_id`, `answer`) VALUES
(1, 1, 0, 1, '3'),
(1, 1, 1, 2, '4'),
(1, 1, 0, 3, '5'),
(1, 1, 0, 4, '3.14'),
(1, 2, 1, 1, '121'),
(1, 2, 0, 2, '111'),
(1, 2, 0, 3, '101'),
(1, 2, 0, 4, '122'),
(1, 3, 0, 1, '16'),
(1, 3, 1, 2, '17'),
(1, 3, 0, 3, '18'),
(1, 3, 0, 4, '19');

-- --------------------------------------------------------

--
-- Table structure for table `math_questions`
--

CREATE TABLE `math_questions` (
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `math_questions`
--

INSERT INTO `math_questions` (`test_id`, `question_id`, `question`) VALUES
(1, 1, 'How much is 2+2???'),
(1, 2, 'How much is 11*11?'),
(1, 3, 'How much is 10+7?');

-- --------------------------------------------------------

--
-- Table structure for table `random_answers`
--

CREATE TABLE `random_answers` (
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` smallint(6) NOT NULL DEFAULT '0',
  `answer_id` int(11) NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `random_answers`
--

INSERT INTO `random_answers` (`test_id`, `question_id`, `is_correct`, `answer_id`, `answer`) VALUES
(3, 1, 0, 1, '3'),
(3, 1, 1, 2, '45'),
(3, 1, 0, 3, '42'),
(3, 1, 0, 4, '48'),
(3, 2, 0, 1, 'The Moon'),
(3, 2, 0, 2, 'An Elaphant'),
(3, 2, 1, 3, 'Really Big'),
(3, 2, 0, 4, 'None of the above'),
(3, 3, 1, 1, 'Everest'),
(3, 3, 0, 2, 'Lhotse'),
(3, 3, 0, 3, 'Kanchenjunga'),
(3, 3, 0, 4, 'K2');

-- --------------------------------------------------------

--
-- Table structure for table `random_questions`
--

CREATE TABLE `random_questions` (
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `random_questions`
--

INSERT INTO `random_questions` (`test_id`, `question_id`, `question`) VALUES
(3, 1, 'There are 45 apples. If you pick up 3 apples, how many apples are there?'),
(3, 2, 'The answer is Really Big.'),
(3, 3, 'Before Mount Everest was discovered, what was the tallest mountain on Earth?');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `test_id` int(11) NOT NULL,
  `user_name` text NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports_answers`
--

CREATE TABLE `sports_answers` (
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` smallint(6) NOT NULL DEFAULT '0',
  `answer_id` int(11) NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sports_answers`
--

INSERT INTO `sports_answers` (`test_id`, `question_id`, `is_correct`, `answer_id`, `answer`) VALUES
(2, 1, 0, 1, 'Belgium'),
(2, 1, 1, 2, 'France'),
(2, 1, 0, 3, 'Croatia'),
(2, 1, 0, 4, 'England'),
(2, 2, 0, 1, 'Los Angeles Lakers'),
(2, 2, 0, 2, 'San Antonio Spurs?'),
(2, 2, 1, 3, 'New York Knicks'),
(2, 2, 0, 4, 'Rigas VEF'),
(2, 2, 0, 5, 'Golden State Warriors'),
(2, 2, 0, 6, 'Who is Kristaps Porzingis?'),
(2, 3, 1, 1, '9.58 seconds'),
(2, 3, 0, 2, '9.88 seconds'),
(2, 3, 0, 3, '9.99 seconds'),
(2, 3, 0, 4, '9.28');

-- --------------------------------------------------------

--
-- Table structure for table `sports_questions`
--

CREATE TABLE `sports_questions` (
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sports_questions`
--

INSERT INTO `sports_questions` (`test_id`, `question_id`, `question`) VALUES
(2, 1, 'Who won the FIFA World Cup 2018?'),
(2, 2, 'Where does Kristaps Porzinigs play?'),
(2, 3, 'What is the men 100 meter sprint world record?');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `test_id` int(11) NOT NULL,
  `test_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`test_id`, `test_name`) VALUES
(1, 'Math'),
(2, 'Sports'),
(3, 'Random');

-- --------------------------------------------------------

--
-- Table structure for table `user_answer`
--

CREATE TABLE `user_answer` (
  `test_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `math_answers`
--
ALTER TABLE `math_answers`
  ADD KEY `test_id` (`test_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `math_questions`
--
ALTER TABLE `math_questions`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `random_answers`
--
ALTER TABLE `random_answers`
  ADD KEY `test_id` (`test_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `random_questions`
--
ALTER TABLE `random_questions`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `sports_answers`
--
ALTER TABLE `sports_answers`
  ADD KEY `test_id` (`test_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `sports_questions`
--
ALTER TABLE `sports_questions`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `user_answer`
--
ALTER TABLE `user_answer`
  ADD KEY `test_id` (`test_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `math_answers`
--
ALTER TABLE `math_answers`
  ADD CONSTRAINT `math_answers_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`),
  ADD CONSTRAINT `math_answers_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `math_questions` (`question_id`);

--
-- Constraints for table `math_questions`
--
ALTER TABLE `math_questions`
  ADD CONSTRAINT `math_questions_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`);

--
-- Constraints for table `random_answers`
--
ALTER TABLE `random_answers`
  ADD CONSTRAINT `random_answers_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`),
  ADD CONSTRAINT `random_answers_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `random_questions` (`question_id`);

--
-- Constraints for table `random_questions`
--
ALTER TABLE `random_questions`
  ADD CONSTRAINT `random_questions_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`);

--
-- Constraints for table `sports_answers`
--
ALTER TABLE `sports_answers`
  ADD CONSTRAINT `sports_answers_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`),
  ADD CONSTRAINT `sports_answers_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `sports_questions` (`question_id`);

--
-- Constraints for table `sports_questions`
--
ALTER TABLE `sports_questions`
  ADD CONSTRAINT `sports_questions_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`);

--
-- Constraints for table `user_answer`
--
ALTER TABLE `user_answer`
  ADD CONSTRAINT `user_answer_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
