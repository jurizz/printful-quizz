<?php 
include_once '../Controller/tests.php';
$tests = new Tests;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <title>Quizz</title>
</head>
<body>
    <main>
        <div class="container">
            
            <form action="../Controller/scriptIndex.php" method="POST">
            <div class="row">
                <label for="user_name">Enter your name:</label>
            </div>
            <div class="row">
                <input type="text" name="user_name" required autofocus>
            </div>
            <div class="row" id="test-select">
                <select class="select" name="selected_test" required>
                    <option disabled selected value>Choose a test</option>
                    <?php $tests->displayTests(); ?>
                </select>
            </div>
            <div class="row" id="start-button">
                <input class="submit" type="submit" value="I am ready!" name="submit">
            </div>
            </form>
        </div>
    </main>
</body>
</html>