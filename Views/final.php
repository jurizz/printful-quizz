<?php session_start() ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <p>Congrats <span class="golden"><?php echo $_SESSION['user_name']; ?></span> your final score is: <span class="golden"><?php echo $_SESSION['score'];?></span></p>
        <a href="index.php">Start again?</a>
    </div>
    <?php 
    include '../Model/dbInsert.php';
    $object = new DbInsert;
    $object->insertDatainResults();
    ?>    
</body>
</html>

<?php session_destroy(); ?>