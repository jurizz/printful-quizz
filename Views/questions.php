<?php session_start(); ?>
<?php 
include_once '../Controller/questions.php';
include_once '../Controller/answers.php';
$question = new Questions;
$answers = new Answers;
$number = (int) $_GET['n'];
$id = (int) $_GET['id'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <title>Questions</title>
    
</head>
<body>
    <main>
        <div class="container">
            <p><?php $question->displayQuestions(); ?></p>
            <form action="../Controller/scriptQuestions.php" method="POST">
            <div class="wrapper">
                <?php $answers->displayAnswers(); ?>
            </div>
                <input class="submit" type="submit" value="NEXT" name="submit">
                <input type="hidden" name="number" value="<?php echo $number; ?>">
                <input type="hidden" name='id' value="<?php echo $id; ?>">
                <input type="hidden" name="correct_answer" value="<?php $answers->displayCorrectAnswer(); ?>">
            </form>
        </div>
    </main>
    <script src="script/app.js" type="text/javascript"></script>
</body>
</html>