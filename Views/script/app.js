let progressBar = document.querySelector('#progress-bar');
let progress = document.querySelector('#progress');
let answers = document.getElementsByClassName('answers');
let answer = document.querySelector('.answers');
let width = 1;


function eventListeners() {
    for (var i = 0; i < answers.length; i++) {
        answers[i].addEventListener("click", myFunction);
    }

}

function clearClass() {
    for (var i = 0; i<answers.length; i++) {
        answers[i].classList.remove("answers-clicked");
    }
}

function myFunction() {
    clearClass();
    this.classList.add("answers-clicked");
}

eventListeners();


