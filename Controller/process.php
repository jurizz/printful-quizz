<?php 

include_once '../Model/dbQuery.php';

class Process extends DbQuery 
{
    //Set variables
    public $question_number;
    public $test_id;
    public $selected_answer;
    public $correct_answer;
    public $submit;
    public $selected_test;
    public $user_name;

    //Construct variables
    function __construct() {
        $this->question_number = isset($_POST['submit']) ? $_POST['number'] : null;
        $this->test_id = isset($_POST['submit']) ? $_POST['id'] : null;
        $this->selected_answer = isset($_POST['submit']) ? $_POST['answer'] : null;
        $this->correct_answer = isset($_POST['submit']) ? $_POST['correct_answer'] : null;
        $this->selected_test = isset($_POST['submit']) ? $_POST['selected_test'] : null;
        $this->user_name = isset($_POST['user_name']) ? $_POST['user_name'] : null;
    }

    //Function to return the next question that can be put in the URL
    public function questionSwitch () {
        $question_number=$this->question_number;
        $next = $question_number+1;
        return $next;
    }

    //Checking Session score, if the session score was not set we set it to zero
    public function checkSessionScore() {
        if(!isset($_SESSION['score'])) {
            $_SESSION['score'] = 0;
            return $_SESSION['score'];
        }
    }

    //Setting session score. If the user picked answer was the correct one we increase the score by 1
    public function setScore () {
        $selected_answer = $this->selected_answer;
        $correct_answer = $this->correct_answer;
        if($selected_answer == $correct_answer) {
            $_SESSION['score']++;
            return $_SESSION['score'];
        }
    }

    //Function to redirect either to the next question or the final page if there are no more questions
    public function questionRedirect() {
        $question_number = $this->question_number;
        $next = $this->questionSwitch();
        $test_id = $this->test_id;
        if($question_number == 3) {
            header('Location: ../Views/final.php');
            exit();
        } else {
            header('Location: ../Views/questions.php?id='.$test_id.'&n='.$next);
        }
    }

    //Setting the session user
    public function setSessionUser() {
        $_SESSION['user_name'] = $this->user_name;
        return $_SESSION['user_name'];
    }

    //Redirecting to questions from the index page
    public function testRedirect() {
        $selected_test = $this->selected_test;
        header('Location: http://localhost/quizz/Views/questions.php?id='.$selected_test.'&n=1');
    }

    //Setting the session test
    public function setSessionTest() {
        $_SESSION['selected_test'] = $this->selected_test;
        return $_SESSION['selected_test'];
    }
}