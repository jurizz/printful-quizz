<?php 

include_once '../Model/dbQuery.php';

class Tests extends DbQuery 
{
    //Display all the tests in the index page
    public function displayTests () {
        $row= $this->getTests();
        foreach($row as $data) {
            echo '<option value="'.$data['test_id'].'">'.$data['test_name'].'</option>';
        }
    }
}