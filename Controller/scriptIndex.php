<?php session_start(); ?>

<?php 

//Render the questions page from the index

include 'process.php';

$object = new Process;
//Add the entered user_name in the session
$object->setSessionUser();
//Add the selected test in the session
$object->setSessionTest();
//Redirect to the questions page
$object->testRedirect();

