<?php 

include_once '../Model/dbQuery.php';

class Answers extends DbQuery 
{

    //Display all the possible answers to the question
    public function displayAnswers () {
        $row = $this->getAnswers();
        foreach ($row as $data) {
            echo '<div class="answers"><label class="label"><input class="radio" type="radio" name="answer" value="'.$data['answer_id'].'" required><span class="answer-span"><strong>'.$data['answer_id'].')</strong> '.$data['answer'].'</span></label></div>';
        }
    }

    //Get the correct answer id for each question
    public function displayCorrectAnswer () {
        $row = $this->getCorrectAnswer();
        foreach ($row as $data) {
            echo $data['answer_id'];
        }
    }
}