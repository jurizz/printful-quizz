<?php session_start(); ?>


<?php 

//Processing the questions

include 'process.php';
include '../Model/dbInsert.php';

$process = new Process;
//Check session score
$process->checkSessionScore();
//Return which question should be next
$process->questionSwitch();
//Set session score to +1 if the answer is correct, if not the score stays at 0
$process->setScore();
$object = new DbInsert;
//Insert the user answer to the question in the database
$object->insertData();
//Redirect to the next question, if it was the last question - redirect to final page
$process->questionRedirect();

