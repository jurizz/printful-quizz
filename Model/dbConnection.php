<?php 

class DbConnection 
{
    //database variables
    private $serverName;
    private $dbName;
    private $userName;
    private $password;

    //database connection
    public function connect() {
        $this->serverName = 'localhost';
        $this->dbName = 'quizz';
        $this->userName = 'root';
        $this->password = '';

        try {
            //data source name
            $dsn = "mysql:host=$this->serverName;dbname=$this->dbName";
            //PDO connection
            $connection = new PDO($dsn, $this->userName, $this->password);
            //setting the PDO error mode to exception
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;
        } catch (PDOException $e) {
            //display error message if connection failed
            echo "Connection failed: ".$e->getMessage();
        }
    }
}