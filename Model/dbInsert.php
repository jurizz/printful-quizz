<?php 

include_once '../Controller/process.php';

class DbInsert extends Process 
{
    public function __construct() {
        parent::__construct();
    }

    //Inserting data about the user selected answer in the database
    public function insertData() {
        $question_number = $this->question_number;
        $test_id = $this->test_id;
        $selected_answer = $this->selected_answer;
        $user_name = $_SESSION['user_name'];
        $query = "INSERT INTO user_answer (test_id, answer_id, question_id, user_name) VALUES (:test_id, :selected_answer, :question_number, :user_name)";
        $statement = $this->connect()->prepare($query);
        $statement->bindParam(':test_id', $test_id);
        $statement->bindParam(':selected_answer', $selected_answer);
        $statement->bindParam(':question_number', $question_number);
        $statement->bindParam(':user_name', $user_name);
        $statement->execute();
    }

    //Inserting data about the test results
    public function insertDatainResults() {
        $test_id = $_SESSION['selected_test'];
        $user_name = $_SESSION['user_name'];
        $score = $_SESSION['score'];
        $query = "INSERT INTO results (test_id, user_name, score) VALUES (:test_id, :user_name, :score)";
        $statement= $this->connect()->prepare($query);
        $statement->bindParam(':test_id', $test_id);
        $statement->bindParam(':user_name', $user_name);
        $statement->bindParam(':score', $score);
        $statement->execute();
    }
    
}