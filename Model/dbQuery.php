<?php

include_once 'dbConnection.php';

class DbQuery extends DbConnection 
{

    //Get questions table from the database
    public function getQuestions() {
        //get superglobals
        $id = (int) $_GET['id'];
        $number = (int) $_GET['n'];
        $query = 'SELECT * FROM math_questions WHERE test_id=:id AND question_id=:number UNION SELECT * FROM sports_questions WHERE test_id=:id AND question_id=:number UNION SELECT * FROM random_questions WHERE test_id=:id AND question_id=:number';
        $statement = $this->connect()->prepare($query);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->bindParam(':number', $number, PDO::PARAM_STR);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    //Get answers table from the database
    public function getAnswers() {
        //get superglobals 
        $number = (int) $_GET['n'];
        $id = (int) $_GET['id'];
        $query = 'SELECT * FROM math_answers WHERE test_id=:id AND question_id=:number UNION SELECT * FROM sports_answers WHERE test_id=:id AND question_id=:number UNION SELECT * FROM random_answers WHERE test_id=:id AND question_id=:number';
        $statement = $this->connect()->prepare($query);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->bindParam(':number', $number, PDO::PARAM_STR);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    //Get tests table from database
    public function getTests() {
        $query = 'SELECT * FROM tests';
        $statement = $this->connect()->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    //Get correct answer id, to later check if selected answer is the correct one
    public function getCorrectAnswer () {
        $number = (int) $_GET['n'];
        $id = (int) $_GET['id'];
        $query = 'SELECT * FROM math_answers WHERE question_id = :number AND test_id= :id AND is_correct=1 UNION SELECT * FROM sports_answers WHERE question_id = :number AND test_id= :id AND is_correct=1 UNION SELECT * FROM random_answers WHERE question_id = :number AND test_id= :id AND is_correct=1';
        $statement = $this->connect()->prepare($query);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->bindParam(':number', $number, PDO::PARAM_STR);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    //Get total amount of rows for each test questions
    public function getTotalRows() {
        $id = (int) $_GET['id'];
        $query = 'SELECT * FROM math_questions WHERE test_id= :id';
        $statement = $this->connect()->prepare($query);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        $total = $statement->rowCount();
        echo $total;
    }

}